import 'dart:io';
import 'package:midterm_test/midterm_test.dart' as midterm_test;

void main(List<String> arguments) {
  // 01
  print("Please input your function");
  String input = stdin.readLineSync()!;
  print(tokenizing(input));
}

List tokenizing(input) {
  // Replace all multiple whitespaces to single whitespace
  final _whitespaceRE = RegExp(r"\s+");
  String formatedInput = input.replaceAll(_whitespaceRE, ' ');
  // Split string to token by single whitespace
  List tokens = formatedInput.split(' ');
  return tokens;
}
